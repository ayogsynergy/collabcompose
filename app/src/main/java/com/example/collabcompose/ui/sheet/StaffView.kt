package com.example.collabcompose.ui.sheet

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View

class StaffView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {

    private var black: Paint
    private var white: Paint

    private val lines = 5

    init {
        black = Paint(Color.BLACK)
        black.style = Paint.Style.FILL
        white = Paint(Color.WHITE)
        white.style = Paint.Style.FILL
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        val lineSpacing: Int = h / lines

        val line = RectF(0f, 0f, 200f, 400f)
    }
}